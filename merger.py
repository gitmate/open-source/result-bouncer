"""
word-wise three way merger
"""
import difflib


def three_way_merge(original, file_a, file_b):
    """
    Performs a word-wise three way merge given three files.

    This doesn't conflict as fast as a line-wise diff and resolves multiple
    in-line changes. It does not, however, tend to produce unpredictable
    output as a character-wise diff tends to do.

    :param original:    Parent file as a iterable of lines
    :param file_a:      First modified file as a iterable of lines
    :param file_b:      Second modified file as a iterable of lines

    :returns:           Resulting file as a list of lines

    :raises ValueError: In case of merge conflict


    """
    if file_a in (original, file_b):
        return file_b
    if file_b in (original, file_a):
        return file_a

    diff_a = _diff(original, file_a)
    diff_b = _diff(original, file_b)

    merged_words = _merge_word_diffs(diff_a, diff_b)

    return _join_words(merged_words)


def _diff(base, modified):
    base_words = ' '.join(base).split(' ')
    modified_words = ' '.join(modified).split(' ')

    diff = list(difflib.Differ().compare(base_words, modified_words))

    # clean context lines which are not part of the file
    clean_diff = [line for line in diff if not line.startswith('?')]

    return clean_diff


def _merge_word_diffs(diff_a, diff_b):
    words = []
    index_a = 0
    index_b = 0

    while index_a < len(diff_a) and index_b < len(diff_b):

        next_a = diff_a[index_a]
        next_b = diff_b[index_b]

        # no changes
        if next_a[0] == ' ' and next_a == next_b:
            words.append(next_a[2:])
            index_a += 1
            index_b += 1

        # addition on both sides
        elif next_a[0] == '+' and next_a == next_b:
            words.append(next_a[2:])
            index_a += 1
            index_b += 1

        # addition in diff_a only
        elif next_a[0] == '+' and (next_b[0] != '+' or next_a == '+ '):
            words.append(next_a[2:])
            index_a += 1

        # addition in diff_b only
        elif next_b[0] == '+' and (next_a[0] != '+' or next_b == '+ '):
            words.append(next_b[2:])
            index_b += 1

        # subtraction
        elif next_a[2:] == next_b[2:] and '-' in (next_a[0], next_b[0]):
            index_a += 1
            index_b += 1

        # conflict
        else:
            raise ValueError('Files are not mergeable without conflict: {}, {}'.format(next_a.__repr__(), next_b.__repr__()))

    # remaining words from either a or b
    for i in range(len(diff_a) - index_a):
        words.append(diff_a[index_a + i][2:])

    for i in range(len(diff_b) - index_b):
        words.append(diff_b[index_b + i][2:])

    return words


def _join_words(words):
    joined = [l+'\n' for l in (' '.join(words)).split('\n ')]
    # remove trailing \n (there might be two or we added one where none was)
    if joined:
        joined[-1] = joined[-1][:-1]
    return joined
