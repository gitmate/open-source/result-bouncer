"""
utility functions for result bouncer
"""


def remove_ranges(contents, ranges):
    """
    remove areas from a file

    runs `remove_range()` for every range in ranges, takes care of overlaps.

    :param contents: list of lines of the original file
    :param ranges:   (start_line, start_column, end_line, end_column)

                     indices start with 1 and can be `None`

    :returns:        list of lines of the modified file
    """
    # SourceRanges must be sorted backwards and overlaps must be eliminated
    # this way, the deletion based on sourceRanges is not offset by
    # previous deletions in the same line that invalidate the indices.
    sranges = []
    previous = []

    for srange in sorted(ranges, reverse=True):
        # no previous
        if not previous:
            previous = srange
        # overlap with previous
        elif list_less_than(srange[:2], previous[2:]) and \
                list_less_than(previous[:2],srange[2:]):
            previous = min(srange[:2], previous[:2]) + \
                           max(srange[2:], previous[2:])
        # previous but no overlap:
        else:
            sranges.append(previous)
            previous = srange

    # last entry
    if previous:
        sranges.append(previous)

    contents = list(contents)

    for srange in sranges:
        contents = remove_range(contents, *srange)

    return contents


def remove_range(contents, start_line, start_column, end_line, end_column):
    """
    remove an area from a file

    :param contents: list of lines of the original file
    :params indices: (start_line, start_column, end_line, end_column)

                     indices start with 1 and can be `None`

    :returns:        list of lines of the modified file
    """
    if not contents:
        return []

    # change to zero based indices and get rid of `None`
    start_line = start_line - 1 if start_line else 0
    start_column = start_column - 1 if start_column else 0
    end_line = end_line - 1 if end_line else len(contents) - 1
    end_column = end_column - 1 if end_column else len(contents[end_line]) - 1

    # let's not modify stuff in place
    newfile = list(contents)

    if start_line == end_line:
        # if it's all in one line, replace the line by it's beginning and end
        newfile[start_line] = (
            newfile[start_line][:start_column]
            + newfile[start_line][end_column + 1:])
        if newfile[start_line] in ('\n', ''):
            del newfile[start_line]
    else:
        # cut away after start
        newfile[start_line] = (
            newfile[start_line][:start_column]+'\n')

        # cut away before end
        newfile[end_line] = (
            newfile[end_line][end_column + 1:])

        # remove lines in the middle
        for i in reversed(range(
                start_line + 1, end_line)):
            del newfile[i]

        # remove leftover empty lines
        # the `start_line +1` here is actually the former `end_line`
        if newfile[start_line + 1] in ('\n', ''):
            del newfile[start_line + 1]
        if newfile[start_line] in ('\n', ''):
            del newfile[start_line]

    # if we deleted the `\n` at the end of a line, we merge it with the
    # next line to stay consistent to what happens in the merger:
    corrected_newfile = []
    line_parts = []
    for line in newfile:
        if not line.endswith('\n'):
            line_parts.append(line)
        elif line_parts:
            line_parts.append(line)
            corrected_newfile.append(" ".join(line_parts))
            line_parts = []
        else:
            corrected_newfile.append(line)

    if line_parts:
        last_line = " ".join(line_parts)+'\n'
        corrected_newfile.append(last_line)

    return corrected_newfile

def list_less_than(a, b):
    """<= for lists/tuples where entries can be None"""
    c = [a, b]
    c.sort(key=lambda x: [y and y or 0 for y in x])
    return a == c[0]
